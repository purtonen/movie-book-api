<?php

use App\Client\Classes\ApiClient;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Http\Environment;
use Slim\Http\Uri;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;

require '/code/vendor/autoload.php';

// Create Slim app
$app = new App();

// Get container
$container = $app->getContainer();

// Register component on container
$container['view'] = function ($container) {
    $view = new Twig(__DIR__ . '/../templates', [
        'cache' => false,
    ]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = Uri::createFromEnvironment(new Environment($_SERVER));
    $view->addExtension(new TwigExtension($router, $uri));

    return $view;
};

// Render the front page of client
$app->get('/', function (Request $request, Response $response, array $args) use ($app) {

    $cookies = $request->getCookieParams();
    $token = $cookies['api_access_token'];
    $params = $request->getQueryParams();
    $data = null;
    $isSearch = false;
    $client = new ApiClient($token);

    // If url parameter 'action' is set, it's a search so we populate data.
    if (isset($params['action']) && $params['action'] === 'getMovie') {
        $title = $params['title'];
        $year = $params['year'];
        $plot = $params['plot'];
        $data = $client->getMovie($title, $year, $plot);
        $isSearch = true;
    } elseif (isset($params['action']) && $params['action'] === 'getBook') {
        $isbn = $params['isbn'];
        $data = $client->getBook($isbn);
        $isSearch = true;
    }

    $tplData = false;
    if ($isSearch && property_exists($data, 'data') && is_object($data->data) && count(get_object_vars($data->data))) {
        if (property_exists($data->data, 'Response') && $data->data->Response === 'False') {
            // OMDB API returns Response=false when no movies are found so we need to empty the set
            $tplData = '';
        } else {
            $tplData = print_r($data->data, true);
        }
    }

    // Render the home.html template with parameters
    return $this->view->render($response, 'home.html', [
        'form'     => [
            'title' => $title,
            'year'  => $year,
            'isbn'  => $isbn,
        ],
        'isSearch' => $isSearch,
        'data'     => $tplData,
    ]);

// Middleware for checking access token cookie. If cookie is not set, redirect to /login
})->add(function (Request $request, Response $response, $next) {

    $cookies = $request->getCookieParams();
    if (!isset($cookies['api_access_token']) || empty($cookies['api_access_token'])) {
        return $response
            ->withStatus(302)
            ->withHeader('Location', '/login');
    }

    // Pass request to next handler
    return $next($request, $response);
});

// Endpoint to set JWT as cookie. The servers /login redirects here with the token as url parameter
$app->get('/setToken', function (Request $request, Response $response, array $args) {
    $token = $request->getQueryParam('token');
    if (isset($token)) {
        return $response
            ->withStatus(302)
            ->withHeader('Location', "/")
            ->withAddedHeader('Set-Cookie', "api_access_token=$token;");
    }
    return $response;
});

// Render a simple login.html login form
$app->get('/login', function (Request $request, Response $response, array $args) {
    return $this->view->render($response, 'login.html', []);
});

// Run the app
$app->run();
