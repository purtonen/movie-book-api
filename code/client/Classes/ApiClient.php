<?php

namespace App\Client\Classes;

use App\Shared\HTTPClient;

class ApiClient extends HTTPClient
{
    function __construct($token)
    {
        parent::__construct('http://php-server.local', ["Authorization: Bearer $token"]);
    }

    function getMovie($title, $year = null, $plot = null)
    {
        return $this->get('/api/getMovie', [
            'title' => $title,
            'year'  => $year,
            'plot'  => $plot,
        ]);
    }

    function getBook($isbn)
    {
        return $this->get("/api/getBook/$isbn");
    }

    // Overwrite HTTPClient's response function to get rid of extra response layers
    protected function response($result)
    {
        $response = (object)[
            'success'      => false,
            'error'        => -1,
            'errorMessage' => 'Unknown error',
            'data'         => null,
        ];

        if ($result !== false) {
            $jsonResponse = json_decode($result);
            $jsonErr = json_last_error();
            if ($jsonErr !== JSON_ERROR_NONE) {
                $jsonErrMsg = json_last_error_msg();
                $response->error--;
                $response->errorMessage = "JSON Error $jsonErr: $jsonErrMsg";

                return $response;
            }

            $response->data = $jsonResponse->data;
            $response->success = true;
            unset($response->error);
            unset($response->errorMessage);
        }

        return $response;
    }
}