<?php

namespace App\Server\Classes;

class JWT
{
    private static $secret, $signAlgo;

    private $expiration;
    private $scopes = [];

    function __construct($expiration = null)
    {
        // Default expiration is after one week
        if ($expiration === null) {
            $expiration = strtotime('+1 week');
        }
        $this->expiration = $expiration;
        $this->scopes[] = 'api_access';
    }

    static function init()
    {
        self::$secret = getenv('JWT_SECRET');
        self::$signAlgo = 'HS256';
    }

    static function validate($tokenString)
    {
        $jwt = \Firebase\JWT\JWT::decode($tokenString, self::$secret, [self::$signAlgo]);
        return $jwt;
    }

    function getExpiration()
    {
        return $this->expiration;
    }

    // Overwrite __toString function to easily use token as string
    function __toString()
    {
        return \Firebase\JWT\JWT::encode([
            'exp'    => $this->expiration,
            'scopes' => $this->scopes,
        ], self::$secret);
    }
}

// Auto-initialize class when loaded
JWT::init();