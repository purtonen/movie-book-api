# Movies and books API

## Prerequisites

Docker must be installed. The app uses docker-compose to run NGINX, PHP-FPM and Composer in containers.


## Installation/setup

1. Clone/copy the repository
2. Copy the .env.example to .env and add required values. `JWT_SECRET` can be any secret string. `ADMIN_HASH` is a bcrypt hash representing the mock admin password used to login, can be any bcrypt hash as long as your password matches it. `OMDB_API_KEY` is an API key that can be generated here: https://www.omdbapi.com/apikey.aspx
3. Modify hosts file (UNIX: /etc/hosts, Windows: C:Windows\System32\Drivers\etc\hosts) to include the following:
    ```
    127.0.0.1 php-server.local
    127.0.0.1 php-client.local
    ```
    This is necessary because we have only one NGINX container that routes request based on the requested URL. 
4. The NGINX Docker container will run on port 8080. If this port is already in use on your machine, you can change it in `docker-compose.yml` service "web" ports on line 7


## Usage

Run `docker-compose up` (optional flag `-d` to detach and free the terminal) in the project root. You can now access the client from `php-client.local:8080` in your browser.

After logging in, you can also copy the generated JWT and use Postman or something similar to access the API directly at `php-server.local:8080` with the JWT as authorization header `Authorization: Bearer {token}`


## Basic infrastructure

The app consists of two distinct parts: server-side and client-side. Server-side is the API that we are actually building and client-side is just to call said API and provide a UI.

### Containers

First layer is the NGINX container that handles incoming traffic and routes it based on the requested URL.
Requests on `php-client.local` and `php-server.local` are routed to a PHP container that then funnel everything to `public/index.php`, where a Slim app is created that then handles all routes.
Before starting the PHP and NGINX containers, the composer container is called upon to install dependencies from composer.json

### App routing (server-side app)

All routes are registered in `Controllers/RouteController`. There we create a route group which consists of the actual endpoints for this app.

The group is then wrapped in a middleware from `Middlewares/AuthorizationMiddleware` that checks for a valid JWT in the Authorization header of the request. No valid JWT, no access and we return 401 Not authorized, 401 Invalid token or 403 Forbidden (valid token but missing access to the API).

Outside of the group and without any middleware is a `/login` endpoint that we can use to generate the JWT. Passing the correct username and password to this endpoint redirects the user to a URL specified in a `redirectTo` parameter passed to the request, adding `?token=xxxxx` as a URL parameter to the redirect.

### App routing (client-side app)

The client-side app has less structure and is more quickly put together since it was not the main point of this app. 

First off when accessing the homepage, we check for an access token cookie. If it's missing, we redirect to the login page, which calls the server-side `/login` endpoint with `redirectTo="http://php-client.local:8080/setToken"`, server-side checks the credentials and redirects to `/setToken` with a JWT as a URL parameter. The parameter is extracted from the URL and set as a cookie along with a redirect back to the homepage. 

Now that the cookie is set, we display the actual client page.

The client page has two simple forms that we can use to make calls to the API with the client-side `Classes/APIClient`.
Returned data is displayed as PHP objects under the forms wrapped in `<pre>` tags.

### Folder structure

All relevant code is inside the `code` folder, which is also the one mounted inside all the docker containers.
Inside `code` we find `client` which holds all client-side code, `server`, which holds all the server-side code and `shared` which holds code shared by both client- and server-side. Practically all the code is shared between containers because of the folder mount, but none of the code is actually used and this app will work all the same if client-side can't access server-side files and vice versa.


## Frameworks and libraries

[Slim framework](http://www.slimframework.com/) is used as the backbone framework for registering routes

[Firebase's PHP-JWT](https://github.com/firebase/php-jwt) for easy JWT generation and validation